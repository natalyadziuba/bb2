from django.shortcuts import render, redirect
from . import forms
from . import models
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User


def main_page(request):

    return render(request, 'main_page.html')


def create_user_view(request):
    form = forms.CreateUser(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],
                                            password=form.cleaned_data['password1'],
                                            email=form.cleaned_data['email'])
            user.save()
            return redirect('core:create_user')

    return render(request, 'create_user_form.html', locals())


def login_user_view(request):
    login_form = forms.CreateLogin(request.POST or None)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)

            return redirect('core:main_page')

    return render(request, 'login_user_form.html', locals())


def edit_user_profile(request, pk):
    user_profile = models.MyUser.objects.get(id=pk)

    return render(request, 'edit_user_profile.html', locals())


def edit_user_password(request):

    return render(request, 'edit_user_password.html', locals())

