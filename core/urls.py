from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.main_page, name='main_page'),
    url(r'^core/create-user/$', views.create_user_view, name='create_user'),
    url(r'^core/login-user/$', views.login_user_view, name='login_user'),
    url(r'^core/edit-user-profile/(?P<pk>\d+)/$', views.edit_user_profile, name='edit_user_profile'),
    url(r'^core/edit-user-password/$', views.edit_user_password, name='edit_user_password'),
]
