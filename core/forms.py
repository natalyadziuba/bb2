from django.contrib.auth.models import User
from django import forms
from .import models
from .models import MyUser


class CreateUser(forms.Form):
    username = forms.CharField(label='Пользователь',
                               max_length=100,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(label='Пароль',
                                max_length=50,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label='Подтвердите пароль',
                                max_length=50,
                                widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    email = forms.CharField(label='Адрес электронной почты',
                            max_length=50,
                            required=True,
                            widget=forms.EmailInput(attrs={'class': 'form-control'}))

    def clean(self):
        cleaned_data = super().clean()
        passwd_1 = cleaned_data.get('password1')
        passwd_2 = cleaned_data.get('password2')

        if passwd_1 and passwd_2 and passwd_1 != passwd_2:
            self.add_error('password1', 'Пароли не совпадают')
            self.add_error('password2', 'Пароли не совпадают')

        return cleaned_data


class CreateLogin(forms.ModelForm):
    class Meta:
        model = MyUser
        fields = '__all__'
