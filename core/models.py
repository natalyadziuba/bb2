from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from django.contrib.auth.models import User


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, **fields):

        fields['email'] = fields['username']

        date_joined = fields.pop('date_joined', None) or timezone.now()
        password = fields.pop('password')
        user = self.model(is_superuser=False, date_joined=date_joined, **fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password):
        if not username:
            raise ValueError('The given username must be set')

        now = timezone.now()

        user = self.model(username=username, email=username, is_staff=True, is_active=True,
                          is_superuser=True, date_joined=now, is_admin=True)
        user.set_password(password)
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser, PermissionsMixin):
    username = models.EmailField('email', unique=True)
    email = models.EmailField('email address', max_length=255, unique=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField('Дата регистрации')

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

